import sys
import xml.etree.cElementTree as ET

source_filename = sys.argv[1]
fout = open(source_filename, 'r')

li = []

tests = 0
failed = 0
ignored = 0

print "Reading contents of "+sys.argv[1]
while True:
	line = fout.readline()
#	print(line)
	li.append(line)
	if line.find('PASS') != -1:
		tests = tests + 1

	if line.find('IGNORE') != -1:
		ignored = ignored + 1
		tests = tests + 1

	pos = line.find('FAIL')
	if pos == 0:
		break
	else:
		if pos != -1:
			failed = failed + 1
			tests = tests + 1

	if line.find('OK') == 0:
		break

fout.close()

#print li
#print tests, failed, ignored



print "parsing Test data"
testsuite = ET.Element("testsuite", tests=str(tests))

for line in li:
	pos = line.find(':test') 	# is it test report line
	if pos != -1:
			numPos = line.find(':')					# first occurence of ':' is after the filename
			dirPos = line.rfind('/', 0, numPos)		# reverser find to '/' to extract test filename 
			testFileName =  line[dirPos+1:numPos]	# slice this and use ass classname for xUnit reporting
			#print testFileName
			passPos = line.find(':PASS') 
			if passPos != -1:
				#print line[pos+5:passPos]  # the name of the test
				doc = ET.SubElement(testsuite, "testcase", classname=testFileName, name=line[pos+5:passPos])

			failPos = line.find(':FAIL') 
			if failPos != -1:
				#print line[pos+5:failPos]  # the name of the test
				doc = ET.SubElement(testsuite, "testcase", classname=testFileName, name=line[pos+5:failPos])
				ET.SubElement(doc, "failure").text = line[failPos+6:]

			ignorePos = line.find(':IGNORE') 
			if ignorePos != -1:
				#print line[pos+5:ignorePos]  # the name of the test
				doc = ET.SubElement(testsuite, "testcase", classname=testFileName, name=line[pos+5:ignorePos])
				ET.SubElement(doc, "skipped")



tree = ET.ElementTree(testsuite)


filename = source_filename[:source_filename.find('.')]+'.xml'
print "Writing to "+filename
tree.write(filename)