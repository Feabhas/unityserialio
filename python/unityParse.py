import serial
import sys
import time
import subprocess
#print 'Number of arguments:', len(sys.argv), 'arguments.'
#print 'Argument List:', str(sys.argv)


import os
pid = os.fork()
if pid > 0:
	print "Opening serial port"
	port = serial.Serial(str(sys.argv[1]), 9600, timeout = 1)
	fout = open('results.txt', 'w')
	#print port
	port.flushInput()
	port.flushOutput()
	while True:
		line = port.readline()
		sys.stdout.write(line)
		sys.stdout.flush()
		fout.write(line)
		if line.find('OK') == 0:
			break
		pos = line.find('FAIL')
		if pos == 0:
			break

	port.close()
	fout.close()
else:
	time.sleep(1)
	print "JLinkExe -commanderscript ../jlink/download.jlink"
	subprocess.call("JLinkExe -commanderscript ../jlink/download.jlink", shell=True)