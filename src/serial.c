/*
 * serial.c
 *
 *  Created on: 24 Feb 2015
 *      Author: niall
 */
#include "stm32f10x.h"		// CMSIS Header
#include "serial.h"

/**
Definitions for USART found in "stm32f10x.h"

typedef struct
{
  __IO uint16_t SR;
  uint16_t  RESERVED0;
  __IO uint16_t DR;
  uint16_t  RESERVED1;
  __IO uint16_t BRR;
  uint16_t  RESERVED2;
  __IO uint16_t CR1;
  uint16_t  RESERVED3;
  __IO uint16_t CR2;
  uint16_t  RESERVED4;
  __IO uint16_t CR3;
  uint16_t  RESERVED5;
  __IO uint16_t GTPR;
  uint16_t  RESERVED6;
} USART_TypeDef;

#define PERIPH_BASE           ((uint32_t)0x40000000)
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
#define USART2_BASE           (APB1PERIPH_BASE + 0x4400)
#define USART2                ((USART_TypeDef *) USART2_BASE)

**/

void putstringU2(const char* str) {
	while(*str != '\0') {
		putcharU2(*str++);
	}
}

// internal function to remap the IO pins for Tx/Rx functionality
static void remap_USART2_IO_Pins();

#if !USING_ST_LIB

// Note that hardcode (magic) numbers have been deliberately used for
// back reference to the STM32F100 Reference Manual
void initUSART2(void) {

	// Remap GPIO PA2 and PA3 as UART2 Tx(PA2) and Rx(PA3)
	remap_USART2_IO_Pins();

	// Set up UASRT2 for 9600,1,N directly
	USART2->CR3 = 0; // ensure no flow control
	USART2->CR2 &= ~(3 << 12); // set to 1 stop bit (CR2.b12|13 = 0)
	USART2->CR1 &= ~(1 << 12); // 8 data bits (CR1.b12 = 0)
	USART2->CR1 &= ~(1 << 10); // no parity   (CR1.b10 = 0)
	USART2->CR1 |= ((1 << 2) | (1 << 3)); // enable Tx and Rx (CR1.b2|b3 = 1)

	// brr value = apb2clk/(16 * baud)
	// given abp2clk is 36MHz and baud is 9600, this is 234.375
	// in [12:4] fixed point this is 0x0EA6
	USART2->BRR = 0x0ea6;	// 9600 baud

	// Enable USART->CR1.b13 = 1 UE: USART enable
	USART2->CR1 |= (1 << 13);
}

void putcharU2(char c) {
	while ((USART2->SR & (1 << 7)) == 0) {
		;
	}
	USART2->DR = c;
}

char getcharU2(void) {
	while ((USART2->SR & (1 << 5)) == 0) {
		;
	}
	return USART2->DR;
}

void remap_USART2_IO_Pins() {
	// Enable PortA Alternative Function IO (AFIO) to remap Tx/Rx from GPIO
	// and enable Gpio_A as UART2 Tx(PA2) Rx(PA3)
	RCC->APB2ENR |= ((1 << 2) | (1 << 0));
	// USART2 is an APB1 Peripheral
	// Enable USART2 clock
	RCC->APB1ENR |= (1 << 17);
	// Set up PA2 for Output
	GPIOA->CRL &= ~(0xFF << (4 * 2));
	GPIOA->CRL |= (0x0B << (4 * 2));
	// Set up PA3 for Input
	GPIOA->CRL &= ~(0xFF << (4 * 3));
	GPIOA->CRL |= (0x04 << (4 * 3));
}

#else  // Using STM32F1 Peripheral Library



void initUSART2(void) {

	// Remap GPIO PA2 and PA3 as UART2 Tx(PA2) and Rx(PA3)
	remap_USART2_IO_Pins();

	// Set up UASRT2 for 9600,1,N using ST Library
	USART_InitTypeDef USART_InitStructure;

	// Defaults to 9600,8,1,N,None - Tx|Rx
	USART_StructInit(&USART_InitStructure);
	USART_Init(USART2, &USART_InitStructure);

	USART_Cmd(USART2, ENABLE);
}

void putcharU2(char c) {
	while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET) {
		;
	}
	USART2->DR = c;
}

char getcharU2(void) {
	while (USART_GetFlagStatus(USART2, USART_FLAG_RXNE) == RESET) {
		;
	}
	return USART2->DR;
}

void remap_USART2_IO_Pins() {
	// Enable PortA Alternative Function IO (AFIO) to remap Tx/Rx from GPIO
	// and enable Gpio_A peripheral clock as UART2 Tx(PA2) Rx(PA3)
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOA, ENABLE);

	// USART2 is an APB1 Peripheral
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	// Set up the GPIO
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_StructInit(&GPIO_InitStruct);

	// Set up PA2 for Tx Output
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	// Set up PA3 for Rx Input
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
}

#endif


