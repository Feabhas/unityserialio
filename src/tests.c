/*
 * tests.c
 *
 *  Created on: 19 Mar 2015
 *      Author: niall
 */

#include "unity.h"


void setUp(void)
{
}

void tearDown(void)
{
}

void testWillPass(void)
{
	TEST_ASSERT_EQUAL(42, 42);
}

void testWillFail(void)
{
	TEST_ASSERT_EQUAL(-1, -1);
}

void testAnotherPassingTest(void)
{
	TEST_ASSERT_EQUAL(23, 23);
}

void testToBeIgnored(void)
{
	TEST_IGNORE_MESSAGE ("The ignore message");
	TEST_ASSERT_EQUAL(23, 23);
}

void testButAddedAnotherFailingTest(void)
{
	TEST_ASSERT_EQUAL(24, 24);
}
