/*
 * serial.h
 *
 *  Created on: 24 Feb 2015
 *      Author: niall
 */

#ifndef DRIVERS_SERIAL_H_
#define DRIVERS_SERIAL_H_

void initUSART2(void);
void putcharU2(char c);
char getcharU2(void);
void putstringU2(const char* str);


#endif /* DRIVERS_SERIAL_H_ */
