/*
 * initialse_hardware.c
 *
 *  Created on: 19 Mar 2015
 *      Author: niall
 */
#include "serial.h"
#include "cmsis_device.h"

void
__initialize_hardware(void)
{
  // Call the CSMSIS system clock routine to store the clock frequency
  // in the SystemCoreClock global RAM location.
  SystemCoreClockUpdate();

  initUSART2();
}
