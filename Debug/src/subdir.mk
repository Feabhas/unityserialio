################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/_write.c \
../src/alternativeTests.c \
../src/initialise_hardware.c \
../src/serial.c \
../src/tests.c \
../src/tests_Runner.c 

OBJS += \
./src/_write.o \
./src/alternativeTests.o \
./src/initialise_hardware.o \
./src/serial.o \
./src/tests.o \
./src/tests_Runner.o 

C_DEPS += \
./src/_write.d \
./src/alternativeTests.d \
./src/initialise_hardware.d \
./src/serial.d \
./src/tests.d \
./src/tests_Runner.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Werror -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../Unity/src/" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


